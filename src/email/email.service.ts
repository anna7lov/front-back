import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';

@Injectable()
export class EmailService {
  constructor(private readonly mailerService: MailerService) {}

  async sendForgotPasswordEmail(
    email: string,
    name: string,
    resetLink: string,
  ): Promise<void> {
    await this.mailerService
      .sendMail({
        to: email,
        subject: 'Password recovery',
        template: `${__dirname}/../email-templates/forgot-password`,
        context: {
          name,
          resetLink,
        },
      })
      .catch((e) => {
        throw new InternalServerErrorException(
          e.message || 'Something went wrong',
        );
      });
  }
}
