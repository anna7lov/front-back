import * as bcrypt from 'bcrypt';
import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import {
  DeleteResult,
  FindManyOptions,
  Repository,
  UpdateResult,
} from 'typeorm';
import { Lector } from './entities/lector.entity';
import { CreateLectorDto } from './dto/create-lector.dto';
import { ILectorWithCourses } from './interfaces/lector-with-courses.interface';
import { UpdateLectorDto } from './dto/update-lector.dto';
import { QueryFilterDto } from 'src/application/dto/query.filter.dto';

@Injectable()
export class LectorsService {
  constructor(
    @InjectRepository(Lector)
    private readonly lectorsRepository: Repository<Lector>,
  ) {}

  async findLectorById(lectorId: number): Promise<Lector | null> {
    const lector = await this.lectorsRepository.findOne({
      where: { id: lectorId },
    });
    return lector;
  }

  async findLectorByEmail(email: string): Promise<Lector | null> {
    const lector = await this.lectorsRepository.findOne({
      where: { email },
    });
    return lector;
  }

  async findCurrentUser(id: number): Promise<Lector> {
    const lector = await this.findLectorById(id);

    if (!lector) {
      throw new NotFoundException(`User was not found`);
    }

    return lector;
  }

  async updateLectorPasswordById(
    id: number,
    password: string,
  ): Promise<UpdateResult> {
    const lector = await this.findLectorById(id);

    if (!lector) {
      throw new NotFoundException('Lector with this id was not found');
    }

    const hashedPassword = await bcrypt.hash(password, 10);

    const result = await this.lectorsRepository.update(id, {
      password: hashedPassword,
    });

    return result;
  }

  async getAllLectors(queryFilter: QueryFilterDto): Promise<any> {
    const query: FindManyOptions<Lector> = {};

    const take = +queryFilter.take || 5;
    const page = +queryFilter.page || 1;
    const skip = (page - 1) * take;

    query.take = take;
    query.skip = skip;

    const [lectors, total] = await this.lectorsRepository.findAndCount(query);

    return { lectors, total };
  }

  async getLectorWithCoursesById(id: number): Promise<ILectorWithCourses> {
    const lector = await this.lectorsRepository
      .createQueryBuilder('lector')
      .leftJoinAndSelect('lector.courses', 'course')
      .where('lector.id = :id', { id })
      .getOne();

    if (!lector) {
      throw new NotFoundException('Lector not found');
    }

    return lector;
  }

  async createLector(createLectorDto: CreateLectorDto): Promise<Lector> {
    const lector = await this.lectorsRepository.findOne({
      where: { email: createLectorDto.email },
    });

    if (lector) {
      throw new BadRequestException('Lector with this email already exists');
    }

    const hashedPassword = await bcrypt.hash(createLectorDto.password, 10);

    const lectorWithHashedPassword = {
      ...createLectorDto,
      password: hashedPassword,
    };

    return this.lectorsRepository.save(lectorWithHashedPassword);
  }

  async updateLectorById(
    id: number,
    updateLectorDto: UpdateLectorDto,
  ): Promise<any> {
    if (updateLectorDto.password) {
      const hashedPassword = await bcrypt.hash(updateLectorDto.password, 10);

      updateLectorDto = {
        ...updateLectorDto,
        password: hashedPassword,
      };
    }

    const updateResult = await this.lectorsRepository.update(
      id,
      updateLectorDto,
    );

    if (updateResult.affected === 1) {
      const updatedLector = await this.lectorsRepository.findOneBy({ id });
      return updatedLector;
    } else {
      return undefined;
    }
  }

  async deleteLectorById(id: number): Promise<DeleteResult> {
    const result = await this.lectorsRepository.delete(id);

    return result;
  }
}
