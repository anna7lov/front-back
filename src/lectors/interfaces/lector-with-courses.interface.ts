import { Lector } from '../entities/lector.entity';
import { Course } from 'src/courses/entities/course.entity';

export interface ILectorWithCourses extends Lector {
  courses: Course[];
}
