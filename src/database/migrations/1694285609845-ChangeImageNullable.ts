import { MigrationInterface, QueryRunner } from 'typeorm';

export class ChangeImageNullable1694285609845 implements MigrationInterface {
  name = 'ChangeImageNullable1694285609845';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "students" ALTER COLUMN "image_path" DROP NOT NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "students" ALTER COLUMN "image_path" SET NOT NULL`,
    );
  }
}
