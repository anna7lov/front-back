import { MigrationInterface, QueryRunner } from 'typeorm';

export class AddSurnameToLectors1693904552134 implements MigrationInterface {
  name = 'AddSurnameToLectors1693904552134';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "lectors" ADD "surname" character varying NOT NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE "lectors" DROP COLUMN "surname"`);
  }
}
