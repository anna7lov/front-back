import { MigrationInterface, QueryRunner } from "typeorm";

export class AddColumnCoursesList1694334265107 implements MigrationInterface {
    name = 'AddColumnCoursesList1694334265107'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "students" ADD "courses_list" integer array`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "students" DROP COLUMN "courses_list"`);
    }

}
