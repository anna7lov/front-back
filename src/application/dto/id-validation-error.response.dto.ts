import { ApiProperty } from '@nestjs/swagger';

export class IdValidationErrorResponseDto {
  @ApiProperty({ example: 'Validation failed (numeric string is expected)' })
  message: string;

  @ApiProperty({ example: 'Bad Request' })
  error: string;

  @ApiProperty({ example: '400' })
  statusCode: number;
}
