import { ApiPropertyOptional } from '@nestjs/swagger';
import { IsString, IsOptional } from 'class-validator';

export class SearchByNameDto {
  @IsOptional()
  @IsString()
  @ApiPropertyOptional({ description: 'Student name to search for' })
  name?: string;
}
