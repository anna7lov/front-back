import { ApiProperty } from '@nestjs/swagger';

export class GetStudentResponseDto {
  @ApiProperty({ description: 'The student auto-generated id', example: '1' })
  id: number;

  @ApiProperty({ description: 'The date and time the student was created' })
  createdAt: Date;

  @ApiProperty({ description: 'The date and time the student was updated' })
  updatedAt: Date;

  @ApiProperty({ description: 'The student name' })
  name: string;

  @ApiProperty({ description: 'The student surname' })
  surname: string;

  @ApiProperty({
    format: 'email',
    description: 'The student email',
  })
  email: string;

  @ApiProperty({ description: 'The student age', example: '20' })
  age: string;

  @ApiProperty({ description: 'The path to student image' })
  imagePath: string;

  @ApiProperty({
    nullable: true,
    description: 'The student group id',
    example: '1',
  })
  groupId: number | null;
}
