import {
  BadRequestException,
  Injectable,
  NestMiddleware,
  NotFoundException,
} from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { StudentsService } from '../students.service';

@Injectable()
export class CheckStudentExistsMiddleware implements NestMiddleware {
  constructor(private readonly studentsService: StudentsService) {}

  async use(request: Request, response: Response, next: NextFunction) {
    const { id } = request.params;

    if (isNaN(+id)) {
      throw new BadRequestException(
        'Validation failed (numeric string is expected)',
      );
    }

    const student = await this.studentsService.findStudentById(+id);

    if (!student) {
      throw new NotFoundException('Student not found');
    }

    next();
  }
}
