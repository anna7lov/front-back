import * as base64 from 'base64-js';
import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { Student } from './entities/student.entity';
import { GroupsService } from 'src/groups/groups.service';
import { CreateStudentDto } from './dto/create-student.dto';
import { UpdateStudentDto } from './dto/update-student.dto';
import { QueryFilterDto } from '../application/dto/query.filter.dto';

@Injectable()
export class StudentsService {
  constructor(
    @InjectRepository(Student)
    private readonly studentsRepository: Repository<Student>,
    private readonly groupsService: GroupsService,
  ) {}

  async findStudentById(studentId: number): Promise<Student | null> {
    const student = await this.studentsRepository.findOne({
      where: { id: studentId },
    });
    return student;
  }

  async getAllStudents(queryFilter: QueryFilterDto): Promise<Student[]> {
    const queryBuilder = this.studentsRepository
      .createQueryBuilder('students')
      .select([
        'students.id as id',
        'students.createdAt as "createdAt"',
        'students.updatedAt as "updatedAt"',
        'students.name as name',
        'students.surname as surname',
        'students.email as email',
        'students.age as age',
        'students.imagePath as "imagePath"',
      ])
      .leftJoin('students.group', 'group')
      .addSelect('group.name as "groupName"');

    if (queryFilter.sort) {
      const sortDirection = queryFilter.order === 'asc' ? 'ASC' : 'DESC';
      queryBuilder.orderBy(`students.${queryFilter.sort}`, sortDirection);
    }

    if (queryFilter.name) {
      queryBuilder.andWhere('students.name = :name', {
        name: queryFilter.name,
      });
    }

    if (queryFilter.surname) {
      queryBuilder.andWhere('students.surname = :surname', {
        surname: queryFilter.surname,
      });
    }

    if (queryFilter.email) {
      queryBuilder.andWhere('students.email = :email', {
        email: queryFilter.email,
      });
    }

    if (queryFilter.groupName) {
      const group = await this.groupsService.findGroupByName(
        queryFilter.groupName,
      );

      if (group) {
        queryBuilder.andWhere('students.groupId = :groupId', {
          groupId: group.id,
        });
      } else {
        return [];
      }
    }

    const students = await queryBuilder.getRawMany();

    return students;
  }

  async getStudentById(id: number): Promise<Student> {
    const student = await this.studentsRepository
      .createQueryBuilder('student')
      .leftJoinAndSelect('student.courses', 'courses')
      .where('student.id = :id', { id })
      .getOne();

    if (!student) {
      throw new NotFoundException('Student not found');
    }

    delete student.coursesList;

    return student;
  }

  async createStudent(createStudentDto: CreateStudentDto): Promise<Student> {
    const student = await this.studentsRepository.findOne({
      where: { email: createStudentDto.email },
    });

    if (student) {
      throw new BadRequestException('Student with this email already exists');
    }

    if (createStudentDto.groupId) {
      const group = await this.groupsService.findGroupById(
        createStudentDto.groupId,
      );

      if (!group) {
        throw new NotFoundException('Group with this id was not found');
      }
    }

    const studentToSave = this.studentsRepository.create(createStudentDto);

    const savedStudent = await this.studentsRepository.save(studentToSave);

    if (createStudentDto.coursesList) {
      const coursesIds = createStudentDto.coursesList;

      for (const courseId of coursesIds) {
        await this.studentsRepository
          .createQueryBuilder()
          .relation('courses')
          .of(savedStudent.id)
          .add(courseId);
      }
    }

    return savedStudent;
  }

  async updateStudentById(
    id: number,
    updateStudentDto: UpdateStudentDto,
  ): Promise<Student> {
    if (updateStudentDto.groupId) {
      const group = await this.groupsService.findGroupById(
        updateStudentDto.groupId,
      );

      if (!group) {
        throw new NotFoundException('Group with this id was not found');
      }
    }

    const existingStudent = await this.studentsRepository
      .createQueryBuilder('student')
      .leftJoinAndSelect('student.courses', 'courses')
      .where('student.id = :id', { id })
      .getOne();

    if (!existingStudent) {
      throw new NotFoundException('Student with this id was not found');
    }

    const currentCourses = existingStudent.courses;
    const newCourseIds = updateStudentDto.coursesList || [];

    for (const courseId of newCourseIds) {
      if (!currentCourses.some((course) => course.id === courseId)) {
        await this.studentsRepository
          .createQueryBuilder()
          .relation('courses')
          .of(existingStudent)
          .add(courseId);
      }
    }

    for (const course of currentCourses) {
      if (!newCourseIds.includes(course.id)) {
        await this.studentsRepository
          .createQueryBuilder()
          .relation('courses')
          .of(existingStudent)
          .remove(course);
      }
    }

    await this.studentsRepository.update(id, updateStudentDto);

    return existingStudent;
  }

  async updateImage(id: number, file: Express.Multer.File): Promise<any> {
    const base64String = base64.fromByteArray(file.buffer);

    const result = await this.studentsRepository.update(id, {
      imagePath: base64String,
    });

    return result;
  }

  async deleteStudentById(id: number): Promise<DeleteResult> {
    const result = await this.studentsRepository.delete(id);

    return result;
  }
}
