import {
  Column,
  Entity,
  Index,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
} from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';
import { Group } from 'src/groups/entities/group.entity';
import { Mark } from 'src/marks/entities/mark.entity';
import { Course } from 'src/courses/entities/course.entity';

@Entity({ name: 'students' })
export class Student extends CoreEntity {
  @Index()
  @Column({
    type: 'varchar',
    nullable: false,
  })
  name: string;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  surname: string;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  email: string;

  @Column({
    type: 'numeric',
    nullable: true,
  })
  age: number;

  @Column({
    name: 'image_path',
    type: 'varchar',
    nullable: true,
  })
  imagePath: string;

  @ManyToOne(() => Group, (group) => group.students, {
    nullable: true,
    eager: false,
    onDelete: 'SET NULL',
  })
  @JoinColumn({ name: 'group_id' })
  group: Group | null;

  @Column({
    type: 'integer',
    nullable: true,
    name: 'group_id',
  })
  groupId: number | null;

  @OneToMany(() => Mark, (mark) => mark.student)
  marks: Mark[];

  @ManyToMany(() => Course, (course) => course.students, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
  })
  @JoinTable({
    name: 'student_course',
    joinColumn: {
      name: 'student_id',
      referencedColumnName: 'id',
    },
    inverseJoinColumn: {
      name: 'course_id',
      referencedColumnName: 'id',
    },
  })
  courses?: Course[];

  @Column({
    type: 'integer',
    nullable: true,
    array: true,
    name: 'courses_list',
  })
  coursesList: number[];
}
