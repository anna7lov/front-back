import {
  Body,
  Controller,
  Delete,
  FileTypeValidator,
  Get,
  HttpCode,
  HttpStatus,
  MaxFileSizeValidator,
  Param,
  ParseFilePipe,
  ParseIntPipe,
  Patch,
  Post,
  Query,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { DeleteResult, UpdateResult } from 'typeorm';
import { AuthGuard } from 'src/auth/guards/auth.guard';
import { StudentsService } from './students.service';
import { Student } from './entities/student.entity';
import { CreateStudentDto } from './dto/create-student.dto';
import { UpdateStudentDto } from './dto/update-student.dto';
import { StudentCreatedResponseDto } from './dto/student-created.response.dto';
import { ServerErrorResponseDto } from 'src/application/dto/server-error.response.dto';
import { IdValidationErrorResponseDto } from 'src/application/dto/id-validation-error.response.dto';
import { ValidationErrorResponseDto } from 'src/application/dto/validation-error.response.dto';
import { NotFoundResponseDto } from 'src/application/dto/not-found.response.dto';
import { AuthErrorResponseDto } from 'src/application/dto/auth-error.response.dto';
import { GetStudentResponseDto } from './dto/get-student.response.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { QueryFilterDto } from '../application/dto/query.filter.dto';

@UseGuards(AuthGuard)
@ApiBearerAuth()
@ApiTags('Students')
@Controller('students')
export class StudentsController {
  constructor(private readonly studentsService: StudentsService) {}

  @ApiOperation({ summary: 'Get all students searchable by name' })
  @ApiResponse({
    status: HttpStatus.OK,
    isArray: true,
    type: GetStudentResponseDto,
    description: 'The list of students',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    type: AuthErrorResponseDto,
    description: 'Authorization required to access',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    type: ServerErrorResponseDto,
    description: 'Internal server error',
  })
  @Get()
  getAllStudents(@Query() queryFilter: QueryFilterDto): Promise<Student[]> {
    return this.studentsService.getAllStudents(queryFilter);
  }

  @ApiOperation({ summary: 'Get student by id' })
  @ApiResponse({
    status: HttpStatus.OK,
    type: GetStudentResponseDto,
    description: 'The student',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    type: IdValidationErrorResponseDto,
    description: 'Id validation  failed',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    type: AuthErrorResponseDto,
    description: 'Authorization required to access',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    type: NotFoundResponseDto,
    description: 'The student was not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    type: ServerErrorResponseDto,
    description: 'Internal server error',
  })
  @Get(':id')
  getStudentById(@Param('id', ParseIntPipe) id: number): Promise<Student> {
    return this.studentsService.getStudentById(id);
  }

  @ApiOperation({ summary: 'Create student' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    type: StudentCreatedResponseDto,
    description: 'The student was created',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    type: ValidationErrorResponseDto,
    description: 'Validation failed',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    type: AuthErrorResponseDto,
    description: 'Authorization required to access',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    type: NotFoundResponseDto,
    description: 'The group was not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    type: ServerErrorResponseDto,
    description: 'Internal server error',
  })
  @Post()
  createStudent(@Body() createStudentDto: CreateStudentDto): Promise<Student> {
    return this.studentsService.createStudent(createStudentDto);
  }

  @ApiOperation({
    summary:
      'Update student by id - ability to update student group and other fields',
  })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'The student was updated',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    type: ValidationErrorResponseDto,
    description: 'Validation failed',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    type: AuthErrorResponseDto,
    description: 'Authorization required to access',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    type: NotFoundResponseDto,
    description: 'The student was not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    type: ServerErrorResponseDto,
    description: 'Internal server error',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @Patch(':id')
  updateStudentById(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateStudentDto: UpdateStudentDto,
  ): Promise<any> {
    return this.studentsService.updateStudentById(id, updateStudentDto);
  }

  @UseInterceptors(FileInterceptor('file'))
  @Patch(':id/image')
  addStudentImageById(
    @Param('id', ParseIntPipe) id: number,
    @UploadedFile(
      new ParseFilePipe({
        validators: [
          new MaxFileSizeValidator({ maxSize: 1024 * 1024 * 10 }),
          new FileTypeValidator({ fileType: '.(png|jpeg|jpg)' }),
        ],
      }),
    )
    file: Express.Multer.File,
  ): Promise<UpdateResult> {
    return this.studentsService.updateImage(id, file);
  }

  @ApiOperation({ summary: 'Delete student by id' })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'The student was deleted',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    type: IdValidationErrorResponseDto,
    description: 'Id validation  failed',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    type: AuthErrorResponseDto,
    description: 'Authorization required to access',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    type: NotFoundResponseDto,
    description: 'The student was not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    type: ServerErrorResponseDto,
    description: 'Internal server error',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @Delete(':id')
  deleteStudentById(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<DeleteResult> {
    return this.studentsService.deleteStudentById(id);
  }
}
