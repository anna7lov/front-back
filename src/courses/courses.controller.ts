import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiOperation,
  ApiResponse,
  ApiTags,
  ApiQuery,
  ApiBearerAuth,
} from '@nestjs/swagger';
import { Course } from './entities/course.entity';
import { AuthGuard } from 'src/auth/guards/auth.guard';
import { DeleteResult, UpdateResult } from 'typeorm';
import { CoursesService } from './courses.service';
import { CreateCourseDto } from './dto/create-course.dto';
import { AddLectorDto } from './dto/add-lector.dto';
import { GetCourseResponseDto } from './dto/get-course.response.dto';
import { CourseCreatedResponseDto } from './dto/course-created.response.dto';
import { ServerErrorResponseDto } from 'src/application/dto/server-error.response.dto';
import { IdValidationErrorResponseDto } from 'src/application/dto/id-validation-error.response.dto';
import { ValidationErrorResponseDto } from 'src/application/dto/validation-error.response.dto';
import { NotFoundResponseDto } from 'src/application/dto/not-found.response.dto';
import { UpdateCourseDto } from './dto/update-course.dto';
import { AuthErrorResponseDto } from 'src/application/dto/auth-error.response.dto';

@UseGuards(AuthGuard)
@ApiBearerAuth()
@ApiTags('Courses')
@Controller('courses')
export class CoursesController {
  constructor(private readonly coursesService: CoursesService) {}

  @ApiOperation({ summary: 'Get courses - all or taught by a certain lector' })
  @ApiQuery({
    name: 'lector_id',
    required: false,
    description: 'The lector id to get courses taught by this lector',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    isArray: true,
    type: GetCourseResponseDto,
    description: 'The list of courses',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    type: IdValidationErrorResponseDto,
    description: 'Id validation  failed',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    type: AuthErrorResponseDto,
    description: 'Authorization required to access',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    type: NotFoundResponseDto,
    description: 'The lector was not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    type: ServerErrorResponseDto,
    description: 'Internal server error',
  })
  @Get()
  getCourses(
    @Query('lector_id', new ParseIntPipe({ optional: true })) lectorId?: number,
  ): Promise<Course[]> {
    if (lectorId) {
      return this.coursesService.getCoursesByLectorId(lectorId);
    } else {
      return this.coursesService.getAllCourses();
    }
  }

  @ApiOperation({ summary: 'Get course by id' })
  @ApiResponse({
    status: HttpStatus.OK,
    type: GetCourseResponseDto,
    description: 'The course',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    type: IdValidationErrorResponseDto,
    description: 'Id validation  failed',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    type: AuthErrorResponseDto,
    description: 'Authorization required to access',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    type: NotFoundResponseDto,
    description: 'The course was not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    type: ServerErrorResponseDto,
    description: 'Internal server error',
  })
  @Get(':id')
  getCourseById(@Param('id', ParseIntPipe) id: number): Promise<Course> {
    return this.coursesService.getCourseById(id);
  }

  @ApiOperation({ summary: 'Create course' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    type: CourseCreatedResponseDto,
    description: 'The course was created',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    type: ValidationErrorResponseDto,
    description: 'Validation failed',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    type: AuthErrorResponseDto,
    description: 'Authorization required to access',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    type: ServerErrorResponseDto,
    description: 'Internal server error',
  })
  @Post()
  createCourse(@Body() createCourseDto: CreateCourseDto): Promise<Course> {
    return this.coursesService.createCourse(createCourseDto);
  }

  @ApiOperation({ summary: 'Add a lector to the course' })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'The lector was added to the course',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    type: ValidationErrorResponseDto,
    description: 'Validation failed',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    type: AuthErrorResponseDto,
    description: 'Authorization required to access',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    type: NotFoundResponseDto,
    description: 'Not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    type: ServerErrorResponseDto,
    description: 'Internal server error',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @Patch(':id/lector')
  addLector(
    @Param('id', ParseIntPipe) id: number,
    @Body() addLectorDto: AddLectorDto,
  ): Promise<void> {
    return this.coursesService.addLector(id, addLectorDto);
  }

  @ApiOperation({
    summary: 'Update course by id',
  })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'The group was updated',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    type: ValidationErrorResponseDto,
    description: 'Validation failed',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    type: AuthErrorResponseDto,
    description: 'Authorization required to access',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    type: NotFoundResponseDto,
    description: 'The course was not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    type: ServerErrorResponseDto,
    description: 'Internal server error',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @Patch(':id')
  updateCourseById(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateCourseDto: UpdateCourseDto,
  ): Promise<UpdateResult> {
    return this.coursesService.updateCourseById(id, updateCourseDto);
  }

  @ApiOperation({ summary: 'Delete course by id' })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'The course was deleted',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    type: IdValidationErrorResponseDto,
    description: 'Id validation  failed',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    type: AuthErrorResponseDto,
    description: 'Authorization required to access',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    type: NotFoundResponseDto,
    description: 'The course was not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    type: ServerErrorResponseDto,
    description: 'Internal server error',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @Delete(':id')
  deleteCourseById(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<DeleteResult> {
    return this.coursesService.deleteCourseById(id);
  }
}
