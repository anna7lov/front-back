import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { CoursesController } from './courses.controller';
import { CoursesService } from './courses.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Course } from './entities/course.entity';
import { LectorsModule } from 'src/lectors/lectors.module';
import { CheckCourseExistsMiddleware } from './middlewares/course.middleware';

@Module({
  imports: [TypeOrmModule.forFeature([Course]), LectorsModule],
  controllers: [CoursesController],
  providers: [CoursesService],
  exports: [CoursesService],
})
export class CoursesModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(CheckCourseExistsMiddleware)
      .forRoutes(
        { path: 'courses/:id/lector', method: RequestMethod.PATCH },
        { path: 'courses/:id', method: RequestMethod.PATCH },
        { path: 'courses/:id', method: RequestMethod.DELETE },
      );
  }
}
