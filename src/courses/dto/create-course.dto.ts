import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString, Max, Min } from 'class-validator';

export class CreateCourseDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: 'The course name' })
  name: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: 'The course description' })
  description: string;

  @IsNumber()
  @Min(1)
  @Max(10000)
  @ApiProperty({ description: 'The course duration in hours', example: '100' })
  hours: number;
}
