import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, Min } from 'class-validator';

export class AddLectorDto {
  @IsNumber()
  @Min(1)
  @ApiProperty({ description: 'The lector id', example: '1' })
  lectorId: number;
}
