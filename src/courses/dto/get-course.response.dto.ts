import { ApiProperty } from '@nestjs/swagger';

export class GetCourseResponseDto {
  @ApiProperty({ description: 'The course auto-generated id', example: '1' })
  id: number;

  @ApiProperty({ description: 'The date and time the course was created' })
  createdAt: Date;

  @ApiProperty({ description: 'The date and time the course was updated' })
  updatedAt: Date;

  @ApiProperty({ description: 'The course name' })
  name: string;

  @ApiProperty({ description: 'The course description' })
  description: string;

  @ApiProperty({ description: 'The course duration in hours', example: '100' })
  hours: string;
}
