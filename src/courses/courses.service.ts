import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository, UpdateResult } from 'typeorm';
import { Course } from './entities/course.entity';
import { LectorsService } from 'src/lectors/lectors.service';
import { CreateCourseDto } from './dto/create-course.dto';
import { AddLectorDto } from './dto/add-lector.dto';
import { UpdateCourseDto } from './dto/update-course.dto';

@Injectable()
export class CoursesService {
  constructor(
    @InjectRepository(Course)
    private readonly coursesRepository: Repository<Course>,
    private readonly lectorsService: LectorsService,
  ) {}

  async findCourseById(courseId: number): Promise<Course | null> {
    const course = await this.coursesRepository.findOne({
      where: { id: courseId },
    });
    return course;
  }

  async courseLectorRelationExists(
    courseId: number,
    lectorId: number,
  ): Promise<number> {
    const count = await this.coursesRepository
      .createQueryBuilder('course')
      .leftJoin('course.lectors', 'lector')
      .where('course.id = :courseId', { courseId })
      .andWhere('lector.id = :lectorId', { lectorId })
      .getCount();
    return count;
  }

  async getAllCourses(): Promise<Course[]> {
    const query = this.coursesRepository
      .createQueryBuilder('course')
      .leftJoinAndSelect('course.students', 'students')
      .select([
        'course.id AS id',
        'course.createdAt AS "createdAt"',
        'course.updatedAt AS "updatedAt"',
        'course.name AS name',
        'course.description AS description',
        'course.hours AS hours',
        'COUNT(students.id) AS "studentsCount"',
      ])
      .groupBy('course.id');

    return await query.getRawMany();
  }

  async getCoursesByLectorId(lectorId: number): Promise<Course[]> {
    const courses = await this.coursesRepository
      .createQueryBuilder('course')
      .leftJoin('course.lectors', 'lector')
      .where('lector.id = :lectorId', { lectorId })
      .getMany();

    return courses;
  }

  async getCourseById(id: number): Promise<Course> {
    const course = await this.coursesRepository.findOne({ where: { id } });

    if (!course) {
      throw new NotFoundException('Student not found');
    }

    return course;
  }

  async createCourse(createCourseDto: CreateCourseDto): Promise<Course> {
    return this.coursesRepository.save(createCourseDto);
  }

  async addLector(id: number, addLectorDto: AddLectorDto): Promise<void> {
    const lector = await this.lectorsService.findLectorById(
      addLectorDto.lectorId,
    );

    if (!lector) {
      throw new NotFoundException('Lector with this id was not found');
    }

    if (await this.courseLectorRelationExists(id, addLectorDto.lectorId)) {
      throw new BadRequestException('This relation already exists');
    }

    await this.coursesRepository
      .createQueryBuilder('course')
      .relation('lectors')
      .of(id)
      .add(addLectorDto.lectorId);
  }

  async updateCourseById(
    id: number,
    updateCourseDto: UpdateCourseDto,
  ): Promise<UpdateResult> {
    const result = await this.coursesRepository.update(id, updateCourseDto);

    return result;
  }

  async deleteCourseById(id: number): Promise<DeleteResult> {
    const result = await this.coursesRepository.delete(id);

    return result;
  }
}
