import {
  BadRequestException,
  Injectable,
  NestMiddleware,
  NotFoundException,
} from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { CoursesService } from '../courses.service';

@Injectable()
export class CheckCourseExistsMiddleware implements NestMiddleware {
  constructor(private readonly coursesService: CoursesService) {}

  async use(request: Request, response: Response, next: NextFunction) {
    const { id } = request.params;

    if (isNaN(+id)) {
      throw new BadRequestException(
        'Validation failed (numeric string is expected)',
      );
    }

    const course = await this.coursesService.findCourseById(+id);

    if (!course) {
      throw new NotFoundException('Course not found');
    }

    next();
  }
}
