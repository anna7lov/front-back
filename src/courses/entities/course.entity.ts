import { Column, Entity, ManyToMany, OneToMany } from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';
import { Lector } from 'src/lectors/entities/lector.entity';
import { Mark } from 'src/marks/entities/mark.entity';
import { Student } from 'src/students/entities/student.entity';

@Entity({ name: 'courses' })
export class Course extends CoreEntity {
  @Column({
    type: 'varchar',
    nullable: false,
  })
  name: string;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  description: string;

  @Column({
    type: 'numeric',
    nullable: false,
  })
  hours: number;

  @ManyToMany(() => Lector, (lector) => lector.courses, {
    onDelete: 'CASCADE',
    onUpdate: 'NO ACTION',
  })
  lectors: Lector[];

  @OneToMany(() => Mark, (mark) => mark.course)
  marks: Mark[];

  @ManyToMany(() => Student, (student) => student.courses, {
    onDelete: 'NO ACTION',
    onUpdate: 'NO ACTION',
  })
  students?: Student[];
}
