import { Group } from '../entities/group.entity';
import { Student } from 'src/students/entities/student.entity';

export interface IGroupWithStudents extends Group {
  students: Student[];
}
