import { ApiProperty } from '@nestjs/swagger';
import { GetStudentResponseDto } from 'src/students/dto/get-student.response.dto';

export class GetGroupWithStudentsResponseDto {
  @ApiProperty({ description: 'The group auto-generated id', example: '1' })
  id: number;

  @ApiProperty({ description: 'The date and time the group was created' })
  createdAt: Date;

  @ApiProperty({ description: 'The date and time the group was updated' })
  updatedAt: Date;

  @ApiProperty({ description: 'The group name' })
  name: string;

  @ApiProperty({
    type: GetStudentResponseDto,
    isArray: true,
    description: 'The group students',
  })
  students: GetStudentResponseDto[];
}
