import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  UseGuards,
} from '@nestjs/common';
import { GroupsService } from './groups.service';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { DeleteResult, UpdateResult } from 'typeorm';
import { Group } from './entities/group.entity';
import { AuthGuard } from 'src/auth/guards/auth.guard';
import { IGroupWithStudents } from './interfaces/group-with-students.interface';
import { CreateGroupDto } from './dto/create-group.dto';
import { UpdateGroupDto } from './dto/update-group.dto';
import { GroupCreatedResponseDto } from './dto/group-created.response.dto';
import { ServerErrorResponseDto } from 'src/application/dto/server-error.response.dto';
import { IdValidationErrorResponseDto } from 'src/application/dto/id-validation-error.response.dto';
import { NotFoundResponseDto } from 'src/application/dto/not-found.response.dto';
import { ValidationErrorResponseDto } from 'src/application/dto/validation-error.response.dto';
import { AuthErrorResponseDto } from 'src/application/dto/auth-error.response.dto';
import { GetGroupResponseDto } from './dto/get-group.response.dto';
import { GetGroupWithStudentsResponseDto } from './dto/get-group-with-students.response.dto';

@UseGuards(AuthGuard)
@ApiBearerAuth()
@ApiTags('Groups')
@Controller('groups')
export class GroupsController {
  constructor(private readonly groupsService: GroupsService) {}

  @ApiOperation({ summary: 'Get all groups' })
  @ApiResponse({
    status: HttpStatus.OK,
    isArray: true,
    type: GetGroupResponseDto,
    description: 'The list of groups',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    type: AuthErrorResponseDto,
    description: 'Authorization required to access',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    type: ServerErrorResponseDto,
    description: 'Internal server error',
  })
  @Get()
  getAllGroupsWithStudents(): Promise<Group[]> {
    return this.groupsService.getAllGroups();
  }

  @ApiOperation({ summary: 'Get group with students by id' })
  @ApiResponse({
    status: HttpStatus.OK,
    type: GetGroupWithStudentsResponseDto,
    description: 'The group with students by id',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    type: IdValidationErrorResponseDto,
    description: 'Id validation  failed',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    type: AuthErrorResponseDto,
    description: 'Authorization required to access',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    type: NotFoundResponseDto,
    description: 'The group was not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    type: ServerErrorResponseDto,
    description: 'Internal server error',
  })
  @Get(':id')
  getGroupWithStudentsById(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<IGroupWithStudents> {
    return this.groupsService.getGroupWithStudentsById(id);
  }

  @ApiOperation({ summary: 'Create group' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    type: GroupCreatedResponseDto,
    description: 'The group was created',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    type: ValidationErrorResponseDto,
    description: 'Validation failed',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    type: AuthErrorResponseDto,
    description: 'Authorization required to access',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    type: ServerErrorResponseDto,
    description: 'Internal server error',
  })
  @Post()
  createGroup(@Body() createGroupDto: CreateGroupDto): Promise<Group> {
    return this.groupsService.createGroup(createGroupDto);
  }

  @ApiOperation({ summary: 'Update group by id' })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'The group was updated',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    type: ValidationErrorResponseDto,
    description: 'Validation failed',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    type: AuthErrorResponseDto,
    description: 'Authorization required to access',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    type: NotFoundResponseDto,
    description: 'The group was not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    type: ServerErrorResponseDto,
    description: 'Internal server error',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @Patch(':id')
  updateGroupById(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateGroupDto: UpdateGroupDto,
  ): Promise<UpdateResult> {
    return this.groupsService.updateGroupById(id, updateGroupDto);
  }

  @ApiOperation({ summary: 'Delete group by id' })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'The group was deleted',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    type: IdValidationErrorResponseDto,
    description: 'Id validation  failed',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    type: AuthErrorResponseDto,
    description: 'Authorization required to access',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    type: NotFoundResponseDto,
    description: 'The group was not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    type: ServerErrorResponseDto,
    description: 'Internal server error',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @Delete(':id')
  deleteGroupById(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<DeleteResult> {
    return this.groupsService.deleteGroupById(id);
  }
}
