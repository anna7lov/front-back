import {
  BadRequestException,
  Injectable,
  NestMiddleware,
  NotFoundException,
} from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { GroupsService } from '../groups.service';

@Injectable()
export class CheckGroupExistsMiddleware implements NestMiddleware {
  constructor(private readonly groupsService: GroupsService) {}

  async use(request: Request, response: Response, next: NextFunction) {
    const { id } = request.params;

    if (isNaN(+id)) {
      throw new BadRequestException(
        'Validation failed (numeric string is expected)',
      );
    }

    const group = await this.groupsService.findGroupById(+id);

    if (!group) {
      throw new NotFoundException('Group not found');
    }

    next();
  }
}
