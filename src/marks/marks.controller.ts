import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiExtraModels,
  ApiOperation,
  ApiQuery,
  ApiResponse,
  ApiTags,
  refs,
} from '@nestjs/swagger';
import { DeleteResult, UpdateResult } from 'typeorm';
import { AuthGuard } from 'src/auth/guards/auth.guard';
import { MarksService } from './marks.service';
import { Mark } from './entities/mark.entity';
import { IStudentMark } from './interfaces/student-mark.interface';
import { ICourseMark } from './interfaces/course-mark.interface';
import { AddMarkDto } from './dto/add-mark.dto';
import { GetStudentMarkResponseDto } from './dto/get-student-mark.response.dto';
import { GetCourseMarkResponseDto } from './dto/get-course-mark.response.dto';
import { MarkCreatedResponseDto } from './dto/mark-created.response.dto';
import { ServerErrorResponseDto } from 'src/application/dto/server-error.response.dto';
import { IdValidationErrorResponseDto } from 'src/application/dto/id-validation-error.response.dto';
import { ValidationErrorResponseDto } from 'src/application/dto/validation-error.response.dto';
import { NotFoundResponseDto } from 'src/application/dto/not-found.response.dto';
import { UpdateMarkDto } from './dto/update-mark.dto';
import { AuthErrorResponseDto } from 'src/application/dto/auth-error.response.dto';

@UseGuards(AuthGuard)
@ApiBearerAuth()
@ApiTags('Marks')
@Controller('marks')
export class MarksController {
  constructor(private readonly marksService: MarksService) {}

  @ApiOperation({ summary: 'Get course or student marks' })
  @ApiQuery({
    name: 'student_id',
    required: false,
    description:
      'The student id to get student marks. Please note that the query must contain student_id or course_id.',
  })
  @ApiQuery({
    name: 'course_id',
    required: false,
    description:
      'The course id to get course marks. Please note that the query must contain student_id or course_id.',
  })
  @ApiExtraModels(GetStudentMarkResponseDto, GetCourseMarkResponseDto)
  @ApiResponse({
    status: HttpStatus.OK,
    content: {
      'application/json': {
        schema: {
          type: 'array',
          items: {
            oneOf: refs(GetStudentMarkResponseDto, GetCourseMarkResponseDto),
          },
        },
        examples: {
          'Student marks': {
            value: [
              {
                courseName: 'string',
                mark: '10',
              },
            ],
          },
          'Course marks': {
            value: [
              {
                courseName: 'string',
                lectorName: 'string',
                studentName: 'string',
                mark: '10',
              },
            ],
          },
        },
      },
    },
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    type: IdValidationErrorResponseDto,
    description: 'Id validation  failed',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    type: AuthErrorResponseDto,
    description: 'Authorization required to access',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    description: 'Not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    type: ServerErrorResponseDto,
    description: 'Internal server error',
  })
  @Get()
  getMarks(
    @Query('student_id', new ParseIntPipe({ optional: true }))
    studentId?: number,
    @Query('course_id', new ParseIntPipe({ optional: true })) courseId?: number,
  ): Promise<IStudentMark[]> | Promise<ICourseMark[]> {
    if (studentId) {
      return this.marksService.getStudentMarks(studentId);
    } else if (courseId) {
      return this.marksService.getCourseMarks(courseId);
    } else {
      throw new BadRequestException(
        'Query must contain student_id or course_id',
      );
    }
  }

  @ApiOperation({ summary: 'Add mark' })
  @ApiResponse({
    status: HttpStatus.CREATED,
    type: MarkCreatedResponseDto,
    description: 'The mark was added',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    type: ValidationErrorResponseDto,
    description: 'Validation failed',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    type: AuthErrorResponseDto,
    description: 'Authorization required to access',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    type: NotFoundResponseDto,
    description: 'Not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    type: ServerErrorResponseDto,
    description: 'Internal server error',
  })
  @Post()
  addMark(@Body() addMarkDto: AddMarkDto): Promise<Mark> {
    return this.marksService.addMark(addMarkDto);
  }

  @ApiOperation({
    summary: 'Update mark by id',
  })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'The mark was updated',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    type: ValidationErrorResponseDto,
    description: 'Validation failed',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    type: AuthErrorResponseDto,
    description: 'Authorization required to access',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    type: NotFoundResponseDto,
    description: 'The lector was not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    type: ServerErrorResponseDto,
    description: 'Internal server error',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @Patch(':id')
  updateMarkById(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateMarkDto: UpdateMarkDto,
  ): Promise<UpdateResult> {
    return this.marksService.updateMarkById(id, updateMarkDto);
  }

  @ApiOperation({ summary: 'Delete mark by id' })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'The mark was deleted',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    type: IdValidationErrorResponseDto,
    description: 'Id validation  failed',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    type: AuthErrorResponseDto,
    description: 'Authorization required to access',
  })
  @ApiResponse({
    status: HttpStatus.NOT_FOUND,
    type: NotFoundResponseDto,
    description: 'The mark was not found',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    type: ServerErrorResponseDto,
    description: 'Internal server error',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @Delete(':id')
  deleteMarkById(@Param('id', ParseIntPipe) id: number): Promise<DeleteResult> {
    return this.marksService.deleteMarkById(id);
  }
}
