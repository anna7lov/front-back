import { ApiProperty } from '@nestjs/swagger';
import { IsNumber, Max, Min } from 'class-validator';

export class AddMarkDto {
  @IsNumber()
  @Min(0)
  @Max(10)
  @ApiProperty({
    minimum: 0,
    maximum: 10,
    description: 'The mark',
    example: '10',
  })
  mark: number;

  @IsNumber()
  @ApiProperty({ description: 'The course id', example: '1' })
  courseId: number;

  @IsNumber()
  @ApiProperty({ description: 'The student id', example: '1' })
  studentId: number;

  @IsNumber()
  @ApiProperty({ description: 'The lector id', example: '1' })
  lectorId: number;
}
