import { PartialType } from '@nestjs/swagger';
import { AddMarkDto } from './add-mark.dto';

export class UpdateMarkDto extends PartialType(AddMarkDto) {}
