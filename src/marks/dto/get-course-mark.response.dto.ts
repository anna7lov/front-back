import { ApiProperty } from '@nestjs/swagger';

export class GetCourseMarkResponseDto {
  @ApiProperty({
    minimum: 0,
    maximum: 10,
    description: 'The mark',
    example: '10',
  })
  mark: string;

  @ApiProperty({ description: 'The course name' })
  courseName: string;

  @ApiProperty({ description: 'The name of the lector who gave the grade' })
  lectorName: string;

  @ApiProperty({ description: 'The student name' })
  studentName: string;
}
