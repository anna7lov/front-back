import { Mark } from '../entities/mark.entity';

export interface IStudentMark extends Pick<Mark, 'mark'> {
  courseName: string;
}
