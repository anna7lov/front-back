import { Column, Entity, JoinColumn, ManyToOne } from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';
import { Course } from 'src/courses/entities/course.entity';
import { Lector } from 'src/lectors/entities/lector.entity';
import { Student } from 'src/students/entities/student.entity';

@Entity({ name: 'marks' })
export class Mark extends CoreEntity {
  @Column({
    type: 'numeric',
    nullable: false,
  })
  mark: number;

  @ManyToOne(() => Course, (course) => course.marks, {
    nullable: false,
    eager: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'course_id' })
  course: Course;

  @ManyToOne(() => Student, (student) => student.marks, {
    nullable: false,
    eager: false,
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'student_id' })
  student: Student;

  @ManyToOne(() => Lector, (lector) => lector.marks, {
    nullable: true,
    eager: false,
    onDelete: 'SET NULL',
  })
  @JoinColumn({ name: 'lector_id' })
  lector: Lector;

  @Column({
    type: 'integer',
    nullable: false,
    name: 'course_id',
  })
  courseId: number;

  @Column({
    type: 'integer',
    nullable: false,
    name: 'student_id',
  })
  studentId: number;

  @Column({
    type: 'integer',
    nullable: true,
    name: 'lector_id',
  })
  lectorId: number | null;
}
