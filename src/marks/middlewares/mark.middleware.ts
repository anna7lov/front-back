import {
  BadRequestException,
  Injectable,
  NestMiddleware,
  NotFoundException,
} from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import { MarksService } from '../marks.service';

@Injectable()
export class CheckMarkExistsMiddleware implements NestMiddleware {
  constructor(private readonly marksService: MarksService) {}

  async use(request: Request, response: Response, next: NextFunction) {
    const { id } = request.params;

    if (isNaN(+id)) {
      throw new BadRequestException(
        'Validation failed (numeric string is expected)',
      );
    }

    const mark = await this.marksService.findMarkById(+id);

    if (!mark) {
      throw new NotFoundException('Mark not found');
    }

    next();
  }
}
