import { Body, Controller, Post, HttpCode, HttpStatus } from '@nestjs/common';
import { ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { SignUpRequestDto } from './dto/sign-up-request.dto';
import { SignInRequestDto } from './dto/sign-in.request.dto';
import { ForgotPasswordRequestDto } from './dto/forgot-password.request.dto';
import { SignResponseDto } from './dto/sign.response.dto';
import { SignInErrorResponseDto } from 'src/application/dto/sign-in-error.response.dto';
import { ServerErrorResponseDto } from 'src/application/dto/server-error.response.dto';
import { ValidationErrorResponseDto } from 'src/application/dto/validation-error.response.dto';
import { ResetPasswordRequestDto } from './dto/reset-password.request.dto';

@ApiTags('Authentication')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @ApiOperation({ summary: 'Sign in' })
  @ApiResponse({
    status: HttpStatus.OK,
    type: SignResponseDto,
    description: 'Access token',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    type: ValidationErrorResponseDto,
    description: 'Validation failed',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    type: SignInErrorResponseDto,
    description: 'Unauthorized exception',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    type: ServerErrorResponseDto,
    description: 'Internal server error',
  })
  @HttpCode(HttpStatus.OK)
  @Post('sign-in')
  signIn(@Body() signInDto: SignInRequestDto): Promise<SignResponseDto> {
    return this.authService.signIn(signInDto);
  }

  @ApiOperation({ summary: 'Sign up' })
  @ApiResponse({
    status: HttpStatus.OK,
    type: SignResponseDto,
    description: 'Access token',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    type: ValidationErrorResponseDto,
    description: 'Validation failed | Email already exists',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    type: ServerErrorResponseDto,
    description: 'Internal server error',
  })
  @HttpCode(HttpStatus.OK)
  @Post('sign-up')
  signUp(@Body() signUpDto: SignUpRequestDto): Promise<SignResponseDto> {
    return this.authService.signUp(signUpDto);
  }

  @ApiOperation({
    summary: 'Forgot password',
  })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'Password recovery email has been sent',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    type: ValidationErrorResponseDto,
    description: 'Validation failed | Bad request',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    type: ServerErrorResponseDto,
    description: 'Internal server error',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @Post('forgot-password')
  forgotPassword(
    @Body() forgotPasswordDto: ForgotPasswordRequestDto,
  ): Promise<void> {
    return this.authService.forgotPassword(forgotPasswordDto);
  }

  @ApiOperation({
    summary: 'Reset password',
  })
  @ApiResponse({
    status: HttpStatus.NO_CONTENT,
    description: 'Password has been recovered',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    type: ValidationErrorResponseDto,
    description: 'Validation failed | Bad request',
  })
  @ApiResponse({
    status: HttpStatus.INTERNAL_SERVER_ERROR,
    type: ServerErrorResponseDto,
    description: 'Internal server error',
  })
  @HttpCode(HttpStatus.NO_CONTENT)
  @Post('reset-password')
  resetPassword(
    @Body() resetPasswordDto: ResetPasswordRequestDto,
  ): Promise<void> {
    return this.authService.resetPassword(resetPasswordDto);
  }
}
