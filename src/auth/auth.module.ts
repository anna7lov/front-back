import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { JwtModule } from '@nestjs/jwt';
import { AuthController } from './auth.controller';
import { jwtConstants } from 'src/application/constants/auth.constansts';
import { LectorsModule } from 'src/lectors/lectors.module';
import { ResetTokenModule } from 'src/reset-token/reset-token.module';
import { EmailModule } from 'src/email/email.module';

@Module({
  imports: [
    LectorsModule,
    JwtModule.register({
      global: true,
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '50000s' },
    }),
    ResetTokenModule,
    EmailModule,
  ],
  providers: [AuthService],
  controllers: [AuthController],
  exports: [AuthService],
})
export class AuthModule {}
