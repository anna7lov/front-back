import * as bcrypt from 'bcrypt';
import {
  BadRequestException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ResetTokenService } from 'src/reset-token/reset-token.service';
import { LectorsService } from 'src/lectors/lectors.service';
import { EmailService } from 'src/email/email.service';
import { SignResponseDto } from './dto/sign.response.dto';
import { SignInRequestDto } from './dto/sign-in.request.dto';
import { SignUpRequestDto } from './dto/sign-up-request.dto';
import { ForgotPasswordRequestDto } from './dto/forgot-password.request.dto';
import { ResetPasswordRequestDto } from './dto/reset-password.request.dto';

@Injectable()
export class AuthService {
  constructor(
    private lectorsService: LectorsService,
    private jwtService: JwtService,
    private resetTokenService: ResetTokenService,
    private emailService: EmailService,
  ) {}

  async signIn(signInDto: SignInRequestDto): Promise<SignResponseDto> {
    const user = await this.lectorsService.findLectorByEmail(signInDto.email);

    if (!user) {
      throw new UnauthorizedException('Invalid email or password');
    }

    const isPasswordValid = await bcrypt.compare(
      signInDto.password,
      user.password,
    );

    if (!isPasswordValid) {
      throw new UnauthorizedException('Invalid email or password');
    }

    const payload = { userId: user.id };

    return {
      accessToken: await this.jwtService.signAsync(payload),
    };
  }

  async signUp(signUpDto: SignUpRequestDto): Promise<SignResponseDto> {
    if (signUpDto.password !== signUpDto.passwordConfirmation) {
      throw new BadRequestException(`Passwords do not match`);
    }

    const user = await this.lectorsService.createLector(signUpDto);

    const payload = { userId: user.id };

    return {
      accessToken: await this.jwtService.signAsync(payload),
    };
  }

  async forgotPassword(
    forgotPasswordDto: ForgotPasswordRequestDto,
  ): Promise<void> {
    const user = await this.lectorsService.findLectorByEmail(
      forgotPasswordDto.email,
    );

    if (!user) {
      throw new BadRequestException(
        `Cannot generate token for reset password request  because user with email ${forgotPasswordDto.email} is not found`,
      );
    }

    const previousToken = await this.resetTokenService.getResetTokenByLectorId(
      user.id,
    );

    if (previousToken) {
      await this.resetTokenService.deleteResetToken(previousToken.token);
    }

    const resetToken = await this.resetTokenService.generateResetToken(
      forgotPasswordDto.email,
    );

    await this.emailService.sendForgotPasswordEmail(
      forgotPasswordDto.email,
      user.name,
      resetToken.token,
    );
  }

  async resetPassword(
    resetPasswordDto: ResetPasswordRequestDto,
  ): Promise<void> {
    console.log(resetPasswordDto);

    if (resetPasswordDto.password !== resetPasswordDto.passwordConfirmation) {
      throw new BadRequestException(`Passwords do not match`);
    }

    const resetToken = await this.resetTokenService.getResetToken(
      resetPasswordDto.token,
    );

    if (!resetToken) {
      throw new BadRequestException('Reset link is invalid');
    }

    await this.lectorsService.updateLectorPasswordById(
      resetToken.lectorId,
      resetPasswordDto.password,
    );
    await this.resetTokenService.deleteResetToken(resetToken.token);
  }
}
