import { IsEmail, IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class SignInRequestDto {
  @IsEmail()
  @ApiProperty({
    format: 'email',
    description: 'The user email (login)',
  })
  email: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: 'The user password' })
  password: string;
}
