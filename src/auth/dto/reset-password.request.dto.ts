import { IsNotEmpty, IsString, MinLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ResetPasswordRequestDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description: 'JSON Web Token (JWT)',
    example:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjc1LCJpYXQiOjE2OTM1ODk3OTksImV4cCI6MTY5MzU5NDc5OX0.YkjyOZBFzzBYPjAAFgcj0J85MuwFNhKaatIO5W4x6bA',
  })
  token: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(6)
  @ApiProperty({ description: 'The user password' })
  password: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: 'The user password confirmation' })
  passwordConfirmation: string;
}
