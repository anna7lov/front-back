import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString, MinLength } from 'class-validator';

export class SignUpRequestDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: 'The user name' })
  name: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty({ description: 'The user surname' })
  surname: string;

  @IsEmail()
  @ApiProperty({ format: 'email', description: 'The user email (login)' })
  email: string;

  @IsString()
  @MinLength(6)
  @ApiProperty({ description: 'The user password' })
  password: string;

  @IsString()
  @ApiProperty({ description: 'The user password confirmation' })
  passwordConfirmation: string;
}
