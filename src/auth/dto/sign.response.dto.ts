import { IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class SignResponseDto {
  @IsString()
  @IsNotEmpty()
  @ApiProperty({
    description: 'JSON Web Token (JWT)',
    example:
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjc1LCJpYXQiOjE2OTM1ODk3OTksImV4cCI6MTY5MzU5NDc5OX0.YkjyOZBFzzBYPjAAFgcj0J85MuwFNhKaatIO5W4x6bA',
  })
  accessToken: string;
}
