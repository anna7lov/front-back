import { IsEmail } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class ForgotPasswordRequestDto {
  @IsEmail()
  @ApiProperty({
    format: 'email',
    description: 'The user email',
  })
  email: string;
}
