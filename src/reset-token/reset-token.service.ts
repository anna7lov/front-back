import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import * as crypto from 'crypto';
import { InjectRepository } from '@nestjs/typeorm';
import { DeleteResult, Repository } from 'typeorm';
import { ResetToken } from './entities/reset-token.entity';
import { LectorsService } from 'src/lectors/lectors.service';

@Injectable()
export class ResetTokenService {
  private logger: Logger;
  constructor(
    @InjectRepository(ResetToken)
    private resetTokenRepository: Repository<ResetToken>,
    private readonly lectorsService: LectorsService,
  ) {
    this.logger = new Logger(ResetTokenService.name);
  }

  async generateResetToken(email: string): Promise<ResetToken> {
    const token = crypto.randomBytes(32).toString('hex');

    const lector = await this.lectorsService.findLectorByEmail(email);

    if (!lector) {
      throw new NotFoundException('Lector with this id was not found');
    }

    const resetPasswordObject = {
      lectorId: lector.id,
      token,
    };

    return this.resetTokenRepository.save(resetPasswordObject);
  }

  async getResetToken(token: string): Promise<ResetToken> {
    return this.resetTokenRepository.findOne({
      where: { token },
    });
  }

  async getResetTokenByLectorId(id: number): Promise<ResetToken> {
    return this.resetTokenRepository.findOne({
      where: { lectorId: id },
    });
  }

  async deleteResetToken(token: string): Promise<DeleteResult> {
    const result = await this.resetTokenRepository.delete({ token });

    return result;
  }
}
