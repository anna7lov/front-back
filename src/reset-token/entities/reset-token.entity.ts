import { Column, Entity, JoinColumn, OneToOne } from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';
import { Lector } from 'src/lectors/entities/lector.entity';

@Entity({ name: 'reset_tokens' })
export class ResetToken extends CoreEntity {
  @Column({ type: 'varchar' })
  token: string;

  @OneToOne(() => Lector, (lector) => lector.id)
  @JoinColumn({ name: 'lector_id' })
  lector: Lector;

  @Column({
    type: 'integer',
    nullable: false,
    name: 'lector_id',
  })
  lectorId: number;
}
